import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:googlesign/src/pages/user_info.dart';
import 'package:googlesign/src/utilities/common/text_constansts.dart';
import 'package:googlesign/src/utilities/google_authentication.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Spacer(flex: 8),
            Text(
              TextConstansts.clickBelowFor,
              style: textStyle(20),
            ),
            Text(
             TextConstansts.googlesignin,
              style: textStyle(12),
            ),
            const Spacer(),
            ElevatedButton.icon(
              onPressed: () {
                //! direct acces of googlesign
                // GoogleSignIn().signIn();

                //! using provider state management
                final provider =
                    Provider.of<GoogleSignInProvider>(context, listen: false);
                provider.googleLogin(context).then(
                      (value) => Navigator.of(context).push(
                        MaterialPageRoute(builder: (_) {
                          return const UserInformation();
                        }),
                      ),
                    );
              },
              icon: const FaIcon(FontAwesomeIcons.google),
              label: Text(TextConstansts.signIn),
            ),
            const Spacer(flex: 8),
          ],
        ),
      ),
    );
  }
}

TextStyle textStyle(double size) {
  return TextStyle(
    fontSize: size,
    fontWeight: FontWeight.bold,
    color: Colors.orange,
  );
}
