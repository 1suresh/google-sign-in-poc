import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:googlesign/src/utilities/common/text_constansts.dart';
import 'package:googlesign/src/utilities/google_authentication.dart';
import 'package:provider/provider.dart';

class UserInformation extends StatefulWidget {
  const UserInformation({Key? key}) : super(key: key);

  @override
  State<UserInformation> createState() => _UserInformationState();
}

class _UserInformationState extends State<UserInformation> {
  final user = FirebaseAuth.instance.currentUser!;
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size; //screen size
    var fontScalling = MediaQuery.of(context).textScaleFactor; //font responsive
    var notchInset = MediaQuery.of(context).padding; //padding responsive
    var noAnimination =
        MediaQuery.of(context).disableAnimations; //animination disable
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(
            children: [
              const Spacer(),
              Text(
                TextConstansts.profile,
                textScaleFactor: fontScalling * 2,
                style: textStyle(Colors.deepPurple),
              ),
              const Spacer(),
              Column(
                children: [
                  Text(
                    TextConstansts.loggedInSuccessfully,
                    textScaleFactor: fontScalling * 1.4,
                    style: textStyle(Colors.greenAccent),
                  ),
                  Text(
                    '${user.email}',
                    textScaleFactor: fontScalling * 1.6,
                    style: textStyle(Colors.yellow),
                  ),
                ],
              ),
              const Spacer(),
              SizedBox(
                height: screenSize.height / 3,
                width: screenSize.width / 1.8,
                child: Card(
                  child: Column(
                    children: [
                      const Spacer(),
                      Text(
                        TextConstansts.profile,
                        textScaleFactor: fontScalling * 1.3,
                        style: textStyle(Colors.deepPurple),
                      ),
                      const Spacer(),
                      CircleAvatar(
                        radius: screenSize.width / 9,
                        backgroundImage: NetworkImage('${user.photoURL}'),
                      ),
                      const Spacer(),
                      Text(
                        TextConstansts.name + '${user.displayName}',
                        textScaleFactor: fontScalling * 1.2,
                        style: textStyle(Colors.deepPurple),
                      ),
                      Text(
                        TextConstansts.email + '${user.email}',
                        textScaleFactor: fontScalling * 1.0,
                        style: textStyle(Colors.deepPurple),
                      ),
                      const Spacer(flex: 2),
                    ],
                  ),
                ),
              ),
              const Spacer(),
              OutlinedButton(
                onPressed: () {
                  final provider =
                      Provider.of<GoogleSignInProvider>(context, listen: false);
                  provider.googleLogout(context);
                  Navigator.pop(context);
                },
                child: Text(
                  'Logout',
                  style: textStyle(Colors.deepPurple),
                ),
              ),
              const Spacer(flex: 7),
            ],
          ),
        ),
      ),
    );
  }

  TextStyle textStyle(Color color) {
    return TextStyle(
      fontWeight: FontWeight.bold,
      color: color,
    );
  }
}
