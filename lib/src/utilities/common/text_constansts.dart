class TextConstansts {
  static String clickBelowFor = 'Click below for';
  static String signIn = 'Sign in';
  static String googlesignin = 'Google Sign-in';
  static String userDetails = 'User Details';
  static String loggedInSuccessfully = 'Logged in successfully for';
  static String profile = 'Profile';
  static String name = 'Name';
  static String email = 'Email';
}
