import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_sign_in/google_sign_in.dart';

//! method-1 not working

// class GoogleSignInProvider extends ChangeNotifier {
//   final GoogleSignIn? googleSingIn = GoogleSignIn();

//   GoogleSignInAccount? _user;
//   GoogleSignInAccount? get user => _user!;

//   Future googleLogin() async {
//     final googleUser = await googleSingIn!.signIn();
//     if (googleUser == null) return;
//     _user = googleUser;

//     final googleAuth = await googleUser.authentication;

//     final credential = GoogleAuthProvider.credential(
//       accessToken: googleAuth.accessToken,
//       idToken: googleAuth.idToken,
//     );
//     await FirebaseAuth.instance.signInWithCredential(credential);
//     notifyListeners();

//   }
// }

//! method 2
// class GoogleSignInProvider extends ChangeNotifier {
//   FirebaseAuth auth = FirebaseAuth.instance;
//   final googleSignIn = GoogleSignIn();

// Future<bool?> googleSignIn() async {
//   GoogleSignInAccount? googleSignInAccount = await gooleSignIn.signIn();

//   if (googleSignInAccount != null) {
//     GoogleSignInAuthentication googleSignInAuthentication =
//         await googleSignInAccount.authentication;

//     AuthCredential credential = GoogleAuthProvider.credential(
//         idToken: googleSignInAuthentication.idToken,
//         accessToken: googleSignInAuthentication.accessToken);

//     UserCredential result = await auth.signInWithCredential(credential);

//     FirebaseAuth user = await auth.currentUser as FirebaseAuth;
//     print(user.tenantId);
//     return Future.value(true);
//   }

// }
FirebaseAuth auth = FirebaseAuth.instance;
final googleSignIn = GoogleSignIn();

class GoogleSignInProvider extends ChangeNotifier {
  FirebaseAuth auth = FirebaseAuth.instance;
  final googleSignIn = GoogleSignIn();
  GoogleSignInAccount? _user;
  //! getter to acces user details
  GoogleSignInAccount? get user => _user;

  Future googleLogin(BuildContext context) async {
    auth = FirebaseAuth.instance;
    final googleSignIn = GoogleSignIn();
    // print("Inside google Signin");
    final googleUser = await googleSignIn.signIn();
    if (googleUser == null) return;
    //! assigning user to acces where ever we needed
    _user = googleUser;
    final googleAuth = await googleUser.authentication;
    final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);
    // print("After credentials");
    var authResult =
        await FirebaseAuth.instance.signInWithCredential(credential);
    notifyListeners();
  }

  googleLogout(BuildContext context) {
    googleSignIn.signOut();
  }
}
